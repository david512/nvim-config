--              _   _
--   ___  _ __ | |_(_) ___  _ __  ___
--  / _ \| '_ \| __| |/ _ \| '_ \/ __|
-- | (_) | |_) | |_| | (_) | | | \__ \
--  \___/| .__/ \__|_|\___/|_| |_|___/
--       |_|


local o = vim.opt
--local opt = vim.opt
-- local g   = vim.g

o.backup = false                          -- creates a backup file
o.writebackup = false                     -- if a file is being edited by another program (or was written to file while editing with another program), it is not allowed to be edited
-- o.undofile = true                         -- enable persistent undo
o.swapfile = false                        -- creates a swapfile
o.history = 50                            -- Remember 50 items in commandline history
o.clipboard = "unnamedplus"               -- allows neovim to access the system clipboard
o.cmdheight = 2                          -- more space in the neovim command line for displaying messages
o.completeopt = { "menuone", "noselect" } -- mostly just for cmp
o.conceallevel = 0                        -- so that `` is visible in markdown files
o.fileencoding = "utf-8"                  -- the encoding written to a file
o.hlsearch = true                         -- highlight all matches on previous search pattern
o.ignorecase = true                       -- case insensitive searching...
o.smartcase = true                        -- ...unless /C or capital in search
o.mouse = "a"                             -- allow the mouse to be used in neovim
o.mousemodel = "popup"
o.pumheight = 10                          -- pop up menu height
o.showmode = false                        -- we don't need to see things like -- INSERT -- anymore
o.showtabline = 0                         -- always show tabs
o.smartindent = true                      -- make indenting smarter again
o.splitbelow = true                       -- force all horizontal splits to go below current window
o.splitright = true                       -- force all vertical splits to go to the right of current window
o.termguicolors = true                    -- set term gui colors (most terminals support this)
-- o.background = 'dark'
o.timeoutlen = 1000                       -- time to wait for a mapped sequence to complete (in milliseconds)
o.updatetime = 300                        -- faster completion (4000ms default)
o.expandtab = true                        -- convert tabs to spaces
o.smarttab = true
o.shiftwidth = 2                          -- the number of spaces inserted for each indentation
o.tabstop = 2                             -- insert 2 spaces for a tab
o.textwidth = 300
o.shiftwidth = 4
o.softtabstop = -1                        -- If negative, shiftwidth value is used
o.cursorline = true                       -- highlight the current line
o.number = true                           -- set numbered lines
o.numberwidth = 2                         -- set number column width to 2 {default 4}
-- o.relativenumber = true
-- o.signcolumn = 'number'
o.signcolumn = "yes"                      -- always show the sign column, otherwise it would shift the text each time
o.laststatus = 3
o.showcmd = false
o.ruler = false
o.wrap = false                            -- display lines as one long line
-- o.wrap = true
o.scrolloff = 8                           -- number of screen lines to keep above and below the cursor
o.sidescrolloff = 8
o.guifont = "monospace:h17"               -- the font used in graphical neovim applications
o.fillchars.eob=" "
o.shortmess:append "c"
o.whichwrap:append("<,>,[,],h,l")
o.iskeyword:append("-")

-- Decrease update time
o.timeoutlen = 500
o.updatetime = 200

-- Better editing experience
o.cindent = true
o.autoindent = true
o.list = true -- white signs
o.listchars = 'trail:·,nbsp:◇,tab:→ ,extends:▸,precedes:◂'

-- cmd('syntax on')
-- vim.api.nvim_command('filetype plugin indent on')

-- Do not save when switching buffers
-- o.hidden = true

-- Preserve view while jumping
-- o.jumpoptions = 'view'

-- BUG: this won't update the search count after pressing `n` or `N`
-- When running macros and regexes on a large file, lazy redraw tells neovim/vim not to draw the screen
-- o.lazyredraw = true

-- Better folds (don't fold by default)
-- o.foldmethod = 'indent'
-- o.foldlevelstart = 99
-- o.foldnestmax = 3
-- o.foldminlines = 1

