--   _
--  | | _____ _   _ _ __ ___   __ _ _ __  ___
--  | |/ / _ \ | | | '_ ` _ \ / _` | '_ \/ __|
--  |   <  __/ |_| | | | | | | (_| | |_) \__ \
--  |_|\_\___|\__, |_| |_| |_|\__,_| .__/|___/
--            |___/                |_|

local function map(m, k, v)
    vim.keymap.set(m, k, v, { silent = true })
end

--Remap space as leader key
map("", "<Space>", "<Nop>")
vim.g.mapleader = " "    -- for global vim
-- vim.g.maplocalleader = ' ' -- for local buffer only

-- Modes
--   normal_mode = "n",
--   insert_mode = "i",
--   visual_mode = "v",
--   visual_block_mode = "x",
--   term_mode = "t",
--   command_mode = "c",

-------------------------------------------
-- Quit neovim
map('n', '<C-q>', ':q<CR>')

-- Quick save
map('n', '<C-s>', ':w<CR>')
-- map('i', '<C-z>', '<ESC>ui') -- undo

-- leader-o/O inserts blank line below
map('n', '<leader>o', 'o<ESC>')

-- Move to the next/previous buffer
map('n', '<leader>[', ':bp<CR>')
map('n', '<leader>]', ':bn<CR>')

-- Move to last buffer
map('n', "''", ':b#<CR>')

-- Copying the vscode behaviour of making tab splits
map('n', '<C-\\>', ':vsplit<CR>')
-- map('n', '<A-\\>', ':split<CR>')

-- Better window navigation
map("n", "<C-Left>", "<C-w>h")
map("n", "<C-Down>", "<C-w>j")
map("n", "<C-Up>", "<C-w>k")
map("n", "<C-Right>", "<C-w>l")
map("n", "<C-h>", "<C-w>h")
map("n", "<C-j>", "<C-w>j")
map("n", "<C-k>", "<C-w>k")
map("n", "<C-l>", "<C-w>l")

-- Navigate buffers
map("n", "<S-Right>", ":bnext<CR>")
map("n", "<S-Left>", ":bprevious<CR>")
map("n", "<S-l>", ":bnext<CR>")
map("n", "<S-h>", ":bprevious<CR>")

-- Clear highlights
map("n", "<leader>h", "<cmd>nohlsearch<CR>")

-- Close buffers
-- map("n", "<S-q>", "<cmd>Bdelete!<CR>")

-- Better paste
-- map("v", "p", '"_dP')

-- Insert --
-- Press jk fast to enter
-- keymap("i", "jk", "<ESC>", opts)

-- Visual --
-- Stay in indent mode
-- map("v", "<", "<gv", opts)
-- map("v", ">", ">gv", opts)-- Move line up and down


-- Plugins --

-- NvimTree
-- map('n', '<C-b>', ':NvimTreeToggle<CR>')
map("n", "<leader>e", ":NvimTreeToggle<CR>")

-- Telescope
map("n", "<leader>ff", ":Telescope find_files<CR>")
map("n", "<leader>ft", ":Telescope live_grep<CR>")
map("n", "<leader>fp", ":Telescope projects<CR>")
map("n", "<leader>fb", ":Telescope buffers<CR>")

-- Comment
-- map("n", "<leader>/", "<cmd>lua require('Comment.api').toggle_current_linewise()<CR>", opts)
-- map("x", "<leader>/", '<ESC><CMD>lua require("Comment.api").toggle_linewise_op(vim.fn.visualmode())<CR>')
map('n', '<leader>/', '^i--<Space><ESC><CR>')

--------------------------------------------
map('n', '<A-Down>', ':move .+1<CR>')
map('n', '<A-Up>', ':move .-2<CR>')
map('i', '<A-Down>', '<Esc>:move .+1<CR>==gi')
map('i', '<A-Up>', '<Esc>:move .-2<CR>==gi')
map('x', '<A-Down>', ":move '>+1<CR>gv=gv")
map('x', '<A-Up>', ":move '<-2<CR>gv=gv")
map('n', '<A-j>', ':move .+1<CR>')
map('n', '<A-k>', ':move .-2<CR>')
map('x', '<A-j>', ":move '>+1<CR>gv=gv")
map('x', '<A-k>', ":move '<-2<CR>gv=gv")

-- Use operator pending mode to visually select the whole buffer
-- e.g. dA = delete buffer ALL, yA = copy whole buffer ALL
map('o', 'A', ':<C-U>normal! mzggVG<CR>`z')
map('x', 'A', ':<C-U>normal! ggVG<CR>')

map('n', '<C-E>', ':colorscheme ')

-- Copy to clipboard
-- map('n', '<leader>yy', '"+yy')
---- "nnoremap  <leader>Y  "+yg_
---- "nnoremap  <leader>y  "+y
-- map('v', '<leader>y', '"+y')

-- Paste from clipboard
-- map('n', '<leader>p', '"+p')
-- map('n', '<leader>P', '"+P')
-- map('v', '<leader>p', '"+p')
-- map('v', '<leader>P', '"+P')

-- MIMIC SHELL MOVEMENTS
-- map('i', '<C-E>', '<ESC>A')
-- map('i', '<C-A>', '<ESC>I')
