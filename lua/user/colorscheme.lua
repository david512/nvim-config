-- COLORSCHEMES
-- Uncomment just ONE of the following colorschemes!
-- local colorscheme = "darkplus"
-- local colorscheme = "tokyonight"
-- local colorscheme = 'palenight'
-- local colorscheme = 'base16-dracula'
-- local colorscheme = 'base16-gruvbox-dark-medium'
-- local colorscheme = 'base16-monokai'
-- local colorscheme = 'base16-nord'
-- local colorscheme = 'base16-oceanicnext'
-- local colorscheme = 'base16-ayu-dark'
-- local colorscheme = 'base16-ayu-mirage'
local colorscheme = 'base16-da-one-ocean'
-- local colorscheme = 'base16-onedark'
-- local colorscheme = 'base16-solarized-dark'
-- local colorscheme = 'base16-solarized-light'
-- local colorscheme = 'base16-tomorrow-night'

local status_ok, _ = pcall(vim.cmd, "colorscheme " .. colorscheme)
if not status_ok then
  return
end