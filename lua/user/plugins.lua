--         _             _
--   _ __ | |_   _  __ _(_)_ __  ___
--  | '_ \| | | | |/ _` | | '_ \/ __|
--  | |_) | | |_| | (_| | | | | \__ \
--  | .__/|_|\__,_|\__, |_|_| |_|___/
--  |_|            |___/

local fn = vim.fn

-- Automatically install packer
local install_path = fn.stdpath "data" .. "/site/pack/packer/start/packer.nvim"
if fn.empty(fn.glob(install_path)) > 0 then
    PACKER_BOOTSTRAP = fn.system {
        "git",
        "clone",
        "--depth",
        "1",
        "https://github.com/wbthomason/packer.nvim",
        install_path,
    }
    print "Installing packer close and reopen Neovim..."
    vim.cmd [[packadd packer.nvim]]
end

-- Autocommand that reloads neovim whenever you save the plugins.lua file
vim.cmd [[
  augroup packer_user_config
    autocmd!
    autocmd BufWritePost plugins.lua source <afile> | PackerSync
  augroup end
]]

-- Use a protected call so we don't error out on first use
local status_ok, packer = pcall(require, "packer")
if not status_ok then
    return
end

-- Have packer use a popup window
packer.init {
    display = {
        open_fn = function()
            return require("packer.util").float({ border = "rounded" })
        end,
    },
}

-- Install your plugins here
return packer.startup(function(use)
    -- My plugins here
    use "wbthomason/packer.nvim" -- Have packer manage itself

    use 'nvim-lua/plenary.nvim' -- Useful lua functions used by lots of plugins
    use "windwp/nvim-autopairs" -- Autopairs, integrates with both cmp and treesitter
    use "JoosepAlviste/nvim-ts-context-commentstring"

    use {
        'numToStr/Comment.nvim',
        config = function()
        require('Comment').setup()
        end
    }

    use({'kyazdani42/nvim-web-devicons',
        config = function()
            require('nvim-web-devicons').setup()
        end,
    })

    use({'akinsho/bufferline.nvim',
        config = function()
            require('bufferline').setup()
        end,
    })

    use({'kyazdani42/nvim-tree.lua',
        event = 'CursorHold',
        config = function()
            require('user.plugins.nvim-tree')
        end,
    })

    use({'nvim-lualine/lualine.nvim',
        config = function()
            require('lualine').setup()
        end,
    })
    use "goolord/alpha-nvim"

    -- Colorschemes
    use "lunarvim/darkplus.nvim"
    use "folke/tokyonight.nvim"
    use 'kyazdani42/nvim-palenight.lua'
    use 'RRethy/nvim-base16'

    -- cmp plugins
    use "hrsh7th/nvim-cmp"
    use "hrsh7th/cmp-buffer" -- buffer completions
    use "hrsh7th/cmp-path" -- path completions
    use "saadparwaiz1/cmp_luasnip" -- snippet completions
    use "hrsh7th/cmp-nvim-lsp"
    use "hrsh7th/cmp-nvim-lua"

    -- snippets
    use "L3MON4D3/LuaSnip" --snippet engine
    use "rafamadriz/friendly-snippets"
    
    -- Telescope
    use "nvim-telescope/telescope.nvim"
    
    -- Treesitter
    use "nvim-treesitter/nvim-treesitter"

    -- Automatically set up your configuration after cloning packer.nvim
    -- Put this at the end after all plugins
    if PACKER_BOOTSTRAP then
        require("packer").sync()
    end
end)
