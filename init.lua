--                         _
--   _ __   ___  _____   _(_)_ __ ___
--  | '_ \ / _ \/ _ \ \ / / | '_ ` _ \
--  | | | |  __/ (_) \ V /| | | | | | |
--  |_| |_|\___|\___/ \_/ |_|_| |_| |_|
--

-- local A = vim.api

require 'user.options'
require 'user.keymaps'
require 'user.plugins'
require 'user.autocommands'

require 'user.colorscheme'

---Pretty print lua table
-- function _G.dump(...)
--     local objects = vim.tbl_map(vim.inspect, { ... })
--     print(unpack(objects))
-- end

require "user.plugins.cmp"
require "user.plugins.telescope"
require "user.plugins.autopairs"
require "user.plugins.comment"
require "user.plugins.alpha"
